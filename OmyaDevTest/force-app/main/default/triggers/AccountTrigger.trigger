trigger AccountTrigger on Account (before insert,before update, after insert, after update) {
    
    if(trigger.isBefore && trigger.isInsert)
    {
        AccountHandler.beforeInsert(Trigger.New);
    }

    if(trigger.isAfter && trigger.isInsert)
    {
        AccountHandler.afterInsert(Trigger.newMap);
    }

    if(trigger.isBefore && trigger.isUpdate)
    {
        AccountHandler.beforeUpdate(Trigger.newMap,Trigger.oldMap);
    }

    if(trigger.isAfter && trigger.isUpdate)
    {
        AccountHandler.afterUpdate(Trigger.newMap,Trigger.oldMap);
    }

}