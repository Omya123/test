trigger OpportunuiesdisscusedVersion2 on Discussed_Topic__c (before insert) {
    
    
    List<Discussed_Topic__c> needOpportunities = new List<Discussed_Topic__c>();
    
    for (Discussed_Topic__c diss : trigger.new) {
        if(diss.Create_Opportunity__c == true){
            needOpportunities.add(diss);
        }  
    }
    if (needOpportunities.size() > 0) {
        
        List<Opportunity> newOpportunitys = new List<Opportunity>();
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        Map<String,Discussed_Topic__c> DiscussedTopicsByNameKeys = new Map<String,Discussed_Topic__c>();
        Map<String,OpportunityLineItem> OpportunityLineItemByNameKeys = new Map<String,OpportunityLineItem>();
        
        for (Discussed_Topic__c discTopic : needOpportunities) {
            String AccountString;
            
            if(discTopic.AccountName__c !=null){        //----> Extract first 7 characters from Account Name for Opportunity Naming
                Integer length = discTopic.AccountName__c.length();
                if(length>7){
                    AccountString = discTopic.AccountName__c.substring(0, 7);
                }
                else{
                    AccountString = discTopic.AccountName__c;
                }
                
            }
            if(discTopic.Material__c != null){
                
                String OpportunityNameForProd =  discTopic.Material_Name__c + System.now() + AccountString  ;
                system.debug('OpportunityNameForProd' + OpportunityNameForProd);
                Opportunity opp = new Opportunity(name=OpportunityNameForProd,       //---->Create New Opportunity
                                                  closeDate = system.today() + 30,
                                                  stageName = 'New',
                                                  AccountId = discTopic.VisitAccount__c,
                                                  CurrencyIsoCode = 'CHF',
                                                  Pricebook2Id = discTopic.Price_Book__r.id
                                                 );
                List<PriceBookEntry> priceBookEntries = [SELECT Id, Product2Id, Product2.Id, Product2.Name,UnitPrice
                                                         FROM PriceBookEntry WHERE
                                                         Product2Id =: discTopic.Material__c AND Pricebook2ID =: discTopic.Price_Book__c LIMIT 1];
                if(priceBookEntries.size()>0){
                    system.debug('priceBookEntries' +priceBookEntries);
                    OpportunityLineItem oli = new OpportunityLineItem(); //---->Create OpportunityLineItem.
                    oli.PricebookEntryId = priceBookEntries[0].Id ;
                    oli.Quantity = discTopic.Quantity__c;
                    oli.TotalPrice = discTopic.Quantity__c * priceBookEntries[0].UnitPrice;
                    OpportunityLineItemByNameKeys.put(OpportunityNameForProd,oli);
                }
                newOpportunitys.add(opp);
                
            }
            else{
                
                String OpportunityName = discTopic.Need__c + AccountString ;
                DiscussedTopicsByNameKeys.put(OpportunityName,discTopic);
                system.debug('OpportunityNameForNewReq' + OpportunityName);
                Opportunity opp = new Opportunity(name=OpportunityName,     //---->Create New Opportunity
                                                  closeDate = system.today() + 30,
                                                  stageName = 'New',
                                                  AccountId = discTopic.VisitAccount__c
                                                  //CurrencyIsoCode = 'CHF',
                                                  //Pricebook2Id = discTopic.Price_Book__r.id
                                                 );
                newOpportunitys.add(opp);
            }
        }
        
        insert newOpportunitys;
        system.debug('newOpportunitys' + newOpportunitys);
        for (Opportunity op : newOpportunitys) {           
            if (DiscussedTopicsByNameKeys.containsKey(op.Name)) {           
                DiscussedTopicsByNameKeys.get(op.Name).Opportunity__c = op.Id;
            }
            if (OpportunityLineItemByNameKeys.containsKey(op.Name)) {
                OpportunityLineItemByNameKeys.get(op.Name).OpportunityId = op.Id;
                oliList.add(OpportunityLineItemByNameKeys.get(op.Name));
            }
        }
        insert oliList;
    }
}