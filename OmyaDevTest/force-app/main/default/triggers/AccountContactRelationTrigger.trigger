Trigger AccountContactRelationTrigger on AccountContactRelation (before Delete) {
     if(trigger.IsDelete){ 
         AccountContactTriggerHandler.AccountContactBeforeDelete(Trigger.old); 
    }
  }