trigger CustomerSalesOrgTrigger on Customer_Sales_Org__c (before insert,before update) {
    
    for(Customer_Sales_Org__c cso : Trigger.New)
    {
        if(cso.Customer__r.Customer_SAP_Number__c != null &&
           cso.Sales_Organization__r.Sales_Org_Code__c != null)
        {
        	cso.External_Id__c = cso.Customer__r.Customer_SAP_Number__c + cso.Sales_Organization__r.Sales_Org_Code__c;
    
        }
        else if(cso.Customer__c != null && cso.Sales_Organization__c != null)
        {
            // To be created if first condition fails. Test with Gab
        }
    }
}