trigger ContactTrigger on Contact (before delete) {
    if(trigger.IsDelete){ 
         ContactTriggerHandler.ContactBeforeDelete(Trigger.old); 
    }
   
}