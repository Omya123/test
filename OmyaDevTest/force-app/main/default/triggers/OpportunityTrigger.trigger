trigger OpportunityTrigger on Opportunity (after update, before update, after insert, before insert, before delete , after delete , after undelete) {
    
    if(Trigger.isAfter && Trigger.isUpdate && OpportunityTriggerHandler.run){
        OpportunityTriggerHandler.run = false;
        OpportunityTriggerHandler.MaterialsOfDiffPrincipal(trigger.new, trigger.oldMap);
    }
    /*if(Trigger.isAfter && Trigger.isUpdate){
        
        OpportunityService.handleSegmentLogicAfterClosed(Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        
        OpportunityService.createContractAfterClosedWon(Trigger.newMap, Trigger.oldMap);
        
    }*/
    
}