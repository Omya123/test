trigger DiscussedTopicTrigger on Discussed_Topic__c (after insert , before insert) {
    
    if(trigger.isInsert && trigger.isAfter){   //-AfterInsert
         DiscussedTopicTriggerHandler.DiscussedTopicAfterInsert(Trigger.new); 
    }
    if(trigger.isInsert && trigger.isBefore){  //-BeforeInsert
         DiscussedTopicTriggerHandler.DiscussedTopicBeforeInsert(Trigger.new); 
    }
    
    
}