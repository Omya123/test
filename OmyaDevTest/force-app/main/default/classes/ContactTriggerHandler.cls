public with sharing class ContactTriggerHandler {
    /***********************************************************************************
**********************************
* Author: Chitransh Porwal
* Date: 01/03/2019
* Param: trigger.old
* Return: void
* Description: This method invokes Before Delete and prevent deletion of Primary Contact. 
* Summary of Changes :
*************************************************************************************
*********************************/
    public static void ContactBeforeDelete(List<Contact> conList){
        for(Contact con : conList){
            if(con.Id.equals(con.Account_PrimaryContact__c)){
                con.addError('Primary Contact can\'t be deleted');
            }
        }
    }
}