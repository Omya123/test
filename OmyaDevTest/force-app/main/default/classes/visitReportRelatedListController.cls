public class visitReportRelatedListController {     


    public List<Invoice__c> invoiceAccount {get;set;} 

    public visit_report__c invoice {get;set;} 
    
    public List<Opportunity> Opportunity {get;set;} 
    
    public List<Sample_Trial__c> sampleTrials {get;set;} 
    
     public List<Case> caseList {get;set;} 

     

    //Constructor 

    public visitReportRelatedListController(ApexPages.StandardController controller) { 

        invoice = (visit_report__c)controller.getRecord();
        
        
        
        //
        //visit_report__c acc = [SELECT Account__c FROM visit_report__c WHERE Id = :'a0C1j00000074oPEAQ'];
       // system.debug('invoice' +Acc.Account__c);
       
        //contacts = [SELECT id,region__c,Name, mobilephone, email FROM contact WHERE accountid = :accounts.id ORDER BY Name];     

    } 

      public PageReference GetcustmoerSitunation()
    {
         visit_report__c acc = [SELECT Account__c FROM visit_report__c WHERE Id = :'a0C1j00000074oPEAQ'];
        //visit_report__c acc = [SELECT Account__c FROM visit_report__c WHERE Id = :invoice.Id]; 
        List<Invoice__c> invoiceParentAccount = [SELECT Amount__c,Invoice_Date__c,Name,Status__c FROM Invoice__c WHERE BillToAccount__c = :acc.Account__c];
        
        List<Opportunity> opportunityParentAccount = [SELECT Amount,CloseDate,Name,StageName FROM Opportunity WHERE AccountId = :acc.Account__c];
        
        List<Sample_Trial__c> sampleTrialsParentAccount = [SELECT Name,Status__c,Trial_End__c,Trial_Start__c FROM Sample_Trial__c WHERE Account__c = :acc.Account__c];
        
        List<Case> caseRelatedtoParentAccount = [SELECT CaseNumber,CreatedDate,Status,Subject FROM Case WHERE AccountId = :acc.Account__c];
        
        if(!invoiceParentAccount.isEmpty()) {
           invoiceAccount =  invoiceParentAccount;
        }
        if(!opportunityParentAccount.isEmpty()) {
           Opportunity =  opportunityParentAccount;
        }
        if(!sampleTrialsParentAccount.isEmpty()) {
           sampleTrials =  sampleTrialsParentAccount;
        }
        if(!caseRelatedtoParentAccount.isEmpty()) {
           caseList =  caseRelatedtoParentAccount;
        }

       		
        return null; 
    }
}