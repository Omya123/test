public class AccountContactRelationship {
    @InvocableMethod
    public static void UncheckPrimaryContact(List<AccountContactRelation> recordIds)
    {
		
        system.debug('recordIds'+ recordIds);
        set<id> accIds = new set<Id>();
        for(AccountContactRelation acr : recordIds){
             accIds.add(acr.AccountId);
            
        }
        List<AccountContactRelation> acrList = [SELECT Id, Primary_Che__c, LastModifiedDate, AccountId 
                                                FROM AccountContactRelation where AccountId =: accIds ORDER BY LastModifiedDate DESC OFFSET 1] ;
        List<AccountContactRelation> updatedACR = new List<AccountContactRelation>();
        for(AccountContactRelation acr : acrList){
            acr.Primary_Che__c = false;
            updatedACR.add(acr);
        }
        
        update updatedACR;
        
    }
}