//***********************************************************************************
//**********************************
//* Author: Star Krishna
//* Created Date: 25/02/2019
//* Param: 
//* Return: void
//* Description: This batch process is going to create a task for Opportunity Owner whenever Sample Request delay in the process of requesting and dispatching.
//* Summary of Changes :
//*************************************************************************************//
public class SampleRequestCheckDelayBatch implements Database.Batchable<SObject>,Schedulable
{
        String query;

        public SampleRequestCheckDelayBatch() 
        {
 
            query = 'Select Id,Name, status__c,Requested_Date__c,Dispatched_Date__c,Is_Requested__c,Is_Dispatched__c,Is_Informed_Opportunity_Owner__c,Opportunity__c,Opportunity__r.OwnerID, (SELECT id FROM Sample_Trials__r) from Sample_Request__c where Is_Informed_Opportunity_Owner__c=false AND (status__c =\'Requested\' OR status__c =\'Dispatched\') ';
        }    
        
       public void execute(SchedulableContext sc) {
            Database.executeBatch(this,2000);
       }
       
        public Database.QueryLocator start(Database.BatchableContext bc) {
           
            return Database.getQueryLocator(query);
       }
       
       
       
       public void execute(Database.BatchableContext bc, List<Sample_Request__c> listSampleRequest) {
       
         list<Task> listTask = new list<Task>();
         List<Sample_Request__c> listUpdateSampleRequest=new List<Sample_Request__c>();
         Integer intMaxDuration = Integer.ValueOf(System.Label.Sample_Request_Status_Duration);
       
       try{
       
        for(Sample_Request__c objSampleRequest : listSampleRequest){
        
           ServiceUtility.DisplayDebugProcessDetails(objSampleRequest);         
           if(objSampleRequest.Is_Dispatched__c== false && ServiceUtility.IsExceededSampleRequstDuration(intMaxDuration , objSampleRequest.Requested_Date__c)){
               objSampleRequest.Is_Informed_Opportunity_Owner__c=true;
               listUpdateSampleRequest.add(objSampleRequest);
               Date DueDate = System.today().Adddays(3);
               listTask.add(ServiceUtility.NewTask(objSampleRequest.Name,'Requested Sample Request not dispatched within '+ intMaxDuration + ' days',objSampleRequest.Opportunity__r.OwnerID,objSampleRequest.Id,DueDate ));
               
            }
           if(objSampleRequest.Is_Dispatched__c == true &&  ServiceUtility.IsExceededSampleRequstDuration(intMaxDuration , objSampleRequest.Dispatched_Date__c) && objSampleRequest.Sample_Trials__r.size()==0){
                objSampleRequest.Is_Informed_Opportunity_Owner__c=true;
                listUpdateSampleRequest.add(objSampleRequest);
                Date DueDate = System.today().Adddays(3);
                listTask.add(ServiceUtility.NewTask(objSampleRequest.Name,'Dispatched Sample Request don\'t have Sample trail created within '+ intMaxDuration + ' days',objSampleRequest.Opportunity__r.OwnerID,objSampleRequest.Id,DueDate ));        
            }
        }
        
        System.debug('SAG1936 Testing listTask -'+listTask); 
           
            if(listTask.size()>0)
            {
                insert listTask;
                update listUpdateSampleRequest;
            }    
        }
        catch(Exception exp) {
            System.debug('SAG Checking exception '+exp.getmessage());    
            System.debug('Cause: ' + exp.getCause());    
            System.debug('Line number: ' + exp.getLineNumber());    
            System.debug('Stack trace: ' + exp.getStackTraceString()); 
        }
           
       }
       
       
       
       public void finish(Database.BatchableContext bc) {
          
          system.debug('Dilaram Condition JOB IS FINISHED');
          
       }
    }