@isTest
public class OmyaDataFactory
{
    
    public static List<Account> createAccount()
    {
        
        Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer_Ship_To').getRecordTypeId();
        List<Account> acc = new List<Account> ();
        acc.add(new Account(Name = 'Test',RecordTypeId = RecordTypeId));
        
     //   insert acc;
        return acc;
    }

    public static List<Opportunity> createOpp(Id accId)
    {
       List<Opportunity> opps = new List<Opportunity>();
        
       Opportunity a = new Opportunity(Name= 'OpportunityTest',
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=accId);
       opps.add(a);
      
        
       return opps;
    }
    
    public static List<Product2> createProduct()
    {
      //  Id RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Article').getRecordTypeId();
       List<Product2> pro = new List<Product2>();
        
        // ,RecordTypeId = RecordTypeId
        
        Product2 a = new Product2(Name = 'ProductTest',External_ID__c='abcd');
        pro.add(a);
      
        return pro;
    }
    
    public static List<Pricebook2> createPriceBook()
    {
        List<Pricebook2> pb = new List<Pricebook2>();
        
        Pricebook2 a = new Pricebook2(Name= 'PriceBookTest');
        pb.add(a);
             
        return pb;
    }
    
    public static PricebookEntry createPriceBookEntry(Id pb2, Id pro2)
    {
       // List<PricebookEntry> pbe = new List<PricebookEntry>();
        PricebookEntry pbe = new PricebookEntry(isActive=TRUE, UnitPrice=3, Pricebook2Id=pb2, Product2Id=pro2);    
             
        return pbe;
    }
    
    public static OpportunityLineItem createOLI(Id oId, ID pbeId)
    {
       // List<OpportunityLineItem> oli = new List<OpportunityLineItem>();
        
        OpportunityLineItem o = new OpportunityLineItem();
        o.OpportunityId=oId;
        o.Quantity = 1;
        o.UnitPrice = 2.00; 
        o.PricebookEntryId=pbeId;
        
        return o;
    }
    
}