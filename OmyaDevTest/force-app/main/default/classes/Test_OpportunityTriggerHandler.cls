@isTest(seeAllData=true)
private class Test_OpportunityTriggerHandler
{
    
    static testMethod void TestMaterialsOfDiffPrincipal() {
        
        Account acc = new Account();
        acc.Name = 'Acc Name';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Name';
        opp.AccountId = acc.id;
        opp.StageName='Contratada';
        opp.CloseDate=Date.today();
        insert opp;
        
        Product2 pro =new Product2();
        pro.Name='test';
        pro.productCode='1234';
        pro.isActive = true;
        pro.External_ID__c='abcd';
        insert pro;
        
        PriceBook2 pb2=new PriceBook2();
        pb2.Name = 'test';
        pb2.IsActive = true;
        insert pb2;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.pricebook2Id = pb2.id;
        pbe.product2id = pro.id;
        pbe.unitprice = 1000;
        pbe.isactive = true;
        pbe.UseStandardPrice = false;
        pbe.CurrencyIsoCode = 'CHF';
        insert pbe;
        
        OpportunityLineItem item=new OpportunityLineItem();
        item.OpportunityId = opp.Id;
        item.PricebookEntryId = pbe.Id;
        item.Quantity = 2;
        item.TotalPrice = 10.0;
        
        insert item;
        
        opp.name ='New Opportunity';
        update opp;
    }
}