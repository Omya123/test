public with sharing class OpportunityTriggerHandler {
    public static boolean run = true;
/***********************************************************************************
**********************************
* Author: Chitransh Porwal
* Date: 15/02/2019
* Param: trigger.new & trigger.oldMap
* Return: void
* Description: This method checks the manufacturer of products added to the opportunity and updates a field as per logic on Opportunity.
* Summary of Changes :
*************************************************************************************
*********************************/
    public static void MaterialsOfDiffPrincipal(List<Opportunity> newOpportunitiesList , Map<Id,Opportunity> oldOpportunitiesMap){
        
        for(Opportunity opportunity : newOpportunitiesList){
            if(opportunity.ProductCount__c != oldOpportunitiesMap.get(opportunity.Id).ProductCount__c){
                
                String manufacturer;
                List<Opportunity> OpportunityToUpdate = new List<Opportunity>();
                
                Set<Id> oppIds = new Set<Id>();
                for( Opportunity opportunityId: newOpportunitiesList )
                {
                    oppIds.add( opportunityId.id );
                }
                
                List<Opportunity> oppList = [ Select Id, Materials_are_from_different_principal__c, 
                                             (Select Id, PricebookEntry.Product2.Manufacturer__c,OpportunityId From OpportunityLineItems )
                                             From Opportunity where Id =: oppIds ];
                for(Opportunity oppRecord : oppList){
                    
                    if(oppRecord.OpportunityLineItems.size() > 1){
                        
                        manufacturer = oppRecord.OpportunityLineItems[0].PricebookEntry.Product2.Manufacturer__c;
                        
                        for(OpportunityLineItem item : oppRecord.OpportunityLineItems){
                            if(manufacturer.equals(item.PricebookEntry.Product2.Manufacturer__c)){
                                oppRecord.Materials_are_from_different_principal__c = false;
                            }
                            else{
                                oppRecord.Materials_are_from_different_principal__c = true;
                                break;
                            }
                        }
                        
                    } 
                    else{
                        oppRecord.Materials_are_from_different_principal__c = false;
                        
                    }
                    OpportunityToUpdate.add(oppRecord); 
                }
                if (!OpportunityToUpdate.isEmpty()){
                    
                    update OpportunityToUpdate;
                    
                }   
            }
        }
    }
}