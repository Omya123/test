public with sharing class AccountContactTriggerHandler {
 /***********************************************************************************
**********************************
* Author: Arvind Yadav	
* Date: 01/03/2019
* Param: trigger.old
* Return: void
* Description: This method invokes Before Delete and and remove primary contact from Account while removing relationship. 
* Summary of Changes :
*************************************************************************************
*********************************/
    
    
public static void AccountContactBeforeDelete(List<AccountContactRelation> Accountcontact){
         
List<Account> acc=new list<account>();
Set<ID> setupdateId = new Set<ID>();
if(Trigger.IsDelete)
 {
   for(AccountContactRelation Updatecon: Accountcontact)

  {
   setupdateId.add(Updatecon.Contactid);
   acc =[Select Id,Primary_Contact__c from account where Primary_Contact__c IN:setupdateId] ;
  // system.debug('Size'+acc );
   }
}
  list<Account> oplist = new list<Account>();    
     For (Account ac:Acc)
     {
     ac.Primary_Contact__c=null;
     oplist.add(ac);
      }
   // System.debug('List'+oplist.size());
    Update oplist;
     }
}