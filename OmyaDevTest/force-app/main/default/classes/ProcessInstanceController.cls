public class ProcessInstanceController {
    public String investigationId;
    public String comments {get;set;}
    public List<ProcessInstance> objProcessInstance = new List<ProcessInstance>();
    public ProcessInstanceWorkitem objWorkItem;
    public PageReference redirectPage;
    public boolean isValid {get;set;}
    public List<Approval_Process__c> lstOfApprovalToInsert = new List<Approval_Process__c>();
    
    public ProcessInstanceController()
    {        
        investigationId = ApexPages.currentPage().getParameters().get('id');
        redirectPage = new PageReference('/'+investigationId);
        try{
            if(String.isNotBlank(investigationId) && objProcessInstance.isEmpty()){
                isValid = true; 
                objProcessInstance = [SELECT TargetObjectId,LastActorId,LastActor.Name, CompletedDate FROM ProcessInstance WHERE TargetObjectId = :investigationId];
                objWorkItem = [ SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId = :investigationId LIMIT 1];
            }
        }catch(exception ex){
            isValid = false;
            //ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'No pending approval'));
        }
    }
    public PageReference Approve(){
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setWorkitemId(objWorkItem.Id);
        req.setAction('Approve');
        req.setComments(comments);
        requests.add(req);
        
        Approval.ProcessResult processResults = Approval.process(requests[0]);
        /*if(processResults.getInstanceStatus() == 'Approved' && !objProcessInstance.isEmpty()){
            for(ProcessInstance objProp: objProcessInstance){
                Approval_Process__c objAppProcess = new Approval_Process__c();
                objAppProcess.Approval_User__c = objProp.LastActor.Name;
                //objAppProcess.Approval_Level__c = objProp.StepStatus;
                objAppProcess.Is_Approved__c = true;
                objAppProcess.Comments__c = comments;
                objAppProcess.Date_and_Time_of_approval__c = objProp.CompletedDate;
                objAppProcess.Process_Instance_ID__c = objProp.id;
                lstOfApprovalToInsert.add(objAppProcess);
            }
            if(!lstOfApprovalToInsert.isEmpty()){
                insert lstOfApprovalToInsert;
            }
            redirectPage = new PageReference('/'+lstOfApprovalToInsert[0].id);
            
        }else{
            redirectPage = null; 
        }*/
        return null;
    }
    
    public PageReference Reject(){
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setWorkitemId(objWorkItem.Id);
        req.setAction('Reject');
        req.setComments(comments);
        requests.add(req);
        
        Approval.ProcessResult processResults = Approval.process(requests[0]);
        /*if(processResults.getInstanceStatus() == 'Rejected' && !objProcessInstance.isEmpty()){
            for(ProcessInstance objProp: objProcessInstance){
                Approval_Process__c objAppProcess = new Approval_Process__c();
                objAppProcess.Approval_User__c = objProp.LastActor.Name;
                objAppProcess.Is_Rejected__c = true;
                objAppProcess.Comments__c = comments;
                objAppProcess.Date_and_Time_of_approval__c = objProp.CompletedDate;
                objAppProcess.Process_Instance_ID__c = objProp.id;
                lstOfApprovalToInsert.add(objAppProcess);
            }
            
            if(!lstOfApprovalToInsert.isEmpty()){
                insert lstOfApprovalToInsert;
            }
            redirectPage = new PageReference('/'+lstOfApprovalToInsert[0].id);
            
        }else{
            redirectPage = null; 
        }*/
        return null;
    }
    
    public PageReference Cancel(){
        return null;
    }
}