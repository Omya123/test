global class FutureClass {
	@future
    public static void captureFuture(Set<Id> oppId)
    {  
        try{
            List<Opportunity> OpportunityListToUpdate = [Select Id, Materials_are_from_different_principal__c from Opportunity where Id =: oppId];
            update OpportunityListToUpdate;
        }
        catch(Exception ex){
            
        }
    }
}