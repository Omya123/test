public with sharing class AccountHandler 
{
    public static void beforeInsert(List<Account> accountList)
    {
        Set<String> billingCodeSet = new Set<String>();
        List<Account> setCountryAccountList = new List<Account>();

        for(Account newAccount : accountList)
        {
            if(newAccount.BillingCountryCode != NUll &&
               newAccount.Country__c == Null)
            {
                billingCodeSet.add(newAccount.BillingCountryCode);
                setCountryAccountList.add(newAccount);
            }         
        }

        if(billingCodeSet.size() > 0)
        {
            AccountHandler.setCountry(setCountryAccountList,billingCodeSet);
        }

    }

    public static void setCountry(List<Account> accountList,
                                 Set<String> billingCodeSet)
    {
        if(billingCodeSet.size() > 0)
        {
             List<Country__c> countryList = [SELECT id,ISO_Code__c 
                                             FROM Country__c
                                             WHERE ISO_Code__c
                                             IN :billingCodeSet];
            
            if(countryList.size() > 0 )
            {
                Map<String,Id> isoCodeToCountryIdMap = new Map<String,Id>();

                for(Country__c country : countryList)
                {
                    isoCodeToCountryIdMap.put(country.ISO_Code__c,country.Id);
                }

                for(Account newAccount : accountList)
                {
                    if(isoCodeToCountryIdMap.containsKey(newAccount.BillingCountryCode))
                    {
                        newAccount.Country__c = isoCodeToCountryIdMap.get(newAccount.BillingCountryCode);
                    }
                }
            }
        }
    }

    public static void afterInsert(Map<Id,Account> newAccountMap)
    {

    }

    public static void beforeUpdate(Map<Id,Account> updatedAccountMap,
                                    Map<Id,Account> obsoleteAccountMap)
    {
        Set<String> billingCodeSet = new Set<String>();
        List<Account> setCountryAccountList = new List<Account>();

        for(Account updatedAccount : updatedAccountMap.values())
        {
            if(updatedAccount.BillingCountryCode != NUll &&
               updatedAccount.BillingCountryCode != obsoleteAccountMap.get(updatedAccount.Id).BillingCountryCode)
            {
                billingCodeSet.add(updatedAccount.BillingCountryCode);
                setCountryAccountList.add(updatedAccount);
            }         
        }

        if(billingCodeSet.size() > 0)
        {
            AccountHandler.setCountry(setCountryAccountList,billingCodeSet);
        }
    }

    public static void afterUpdate(Map<Id,Account> updatedAccountMap,
                                    Map<Id,Account> obsoleteAccountMap)
    {
        Set<Id> updatedSAPNoSet = new Set<Id>();

        for(Account updatedAccount : updatedAccountMap.values())
        {
            if(updatedAccount.Customer_SAP_Number__c != NUll &&
               updatedAccount.Customer_SAP_Number__c != obsoleteAccountMap.get(updatedAccount.Id).Customer_SAP_Number__c)
            {
                updatedSAPNoSet.add(updatedAccount.Id);
            }         
        }

        if(updatedSAPNoSet.size() > 0)
        {
            AccountHandler.setExtensions(updatedSAPNoSet);
        }
    }

    private static void setExtensions(Set<Id> accountIdSet)
    {
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT id,Customer_SAP_Number__c,
                                                                 (SELECT id,Sales_Organization__r.Sales_Org_Code__c FROM Customer__r),
                                                                 (SELECT id,Omya_Company__r.OMYA_Company_code__c FROM Customer_Company_Code__r),
                                                                 (SELECT id,Customer__r.Customer_SAP_Number__c,Customer_Sales_Org__r.Sales_Organization__r.Sales_Org_Code__c,PartnerAccount__r.Customer_SAP_Number__c,Function_Type__c FROM Related_Accounts1__r),
                                                                 (SELECT id,Customer__r.Customer_SAP_Number__c,Customer_Sales_Org__r.Sales_Organization__r.Sales_Org_Code__c,PartnerAccount__r.Customer_SAP_Number__c,Function_Type__c FROM Related_Accounts__r)
                                                          FROM Account 
                                                          WHERE id in :accountIdSet]);

        if(accountMap.size() > 0 )
        {
            List<Customer_Sales_Org__c> customerSalesOrgsToUpdate = new List<Customer_Sales_Org__c>();
            List<Customer_Company__c> customerCompaniesToUpdate = new List<Customer_Company__c>();
            Map<Id,Partner_Function__c> partnerFunctionsToUpdate = new Map<Id,Partner_Function__c>();

            for(Account acc : accountMap.values())
            {
                for(Customer_Sales_Org__c customerSalesOrg : acc.Customer__r)
                {
                    customerSalesOrg.External_Id__c = acc.Customer_SAP_Number__c + customerSalesOrg.Sales_Organization__r.Sales_Org_Code__c;
                    customerSalesOrgsToUpdate.add(customerSalesOrg);
                }

                for(Customer_Company__c customerCompany : acc.Customer_Company_Code__r)
                {
                    customerCompany.External_Id__c = acc.Customer_SAP_Number__c + customerCompany.Omya_Company__r.OMYA_Company_code__c;
                    customerCompaniesToUpdate.add(customerCompany);
                }

                for(Partner_Function__c partnerFunction : acc.Related_Accounts1__r)
                {
                    partnerFunction.External_Id__c = partnerFunction.Customer__r.Customer_SAP_Number__c + 
                                                     partnerFunction.PartnerAccount__r.Customer_SAP_Number__c +
                                                     partnerFunction.Customer_Sales_Org__r.Sales_Organization__r.Sales_Org_Code__c + 
                                                     partnerFunction.Function_Type__c;
                    partnerFunctionsToUpdate.put(partnerFunction.id,partnerFunction);
                }

                for(Partner_Function__c partnerFunction : acc.Related_Accounts__r)
                {
                    partnerFunction.External_Id__c = partnerFunction.Customer__r.Customer_SAP_Number__c + 
                                                     partnerFunction.PartnerAccount__r.Customer_SAP_Number__c +
                                                     partnerFunction.Customer_Sales_Org__r.Sales_Organization__r.Sales_Org_Code__c + 
                                                     partnerFunction.Function_Type__c;
                    partnerFunctionsToUpdate.put(partnerFunction.id,partnerFunction);
                }
            } 

            if(customerSalesOrgsToUpdate.size() > 0)
            {
                Database.update(customerSalesOrgsToUpdate,false);
            }

            if(customerCompaniesToUpdate.size() > 0)
            {
                Database.update(customerCompaniesToUpdate,false);
            }
            if(partnerFunctionsToUpdate.size() > 0)
            {
                Database.update(partnerFunctionsToUpdate.values(),false);
            }
        }


    }
}