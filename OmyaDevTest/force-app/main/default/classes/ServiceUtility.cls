public class ServiceUtility {
    
    public static Task NewTask(string subject,Id id){
    
        Task task = new Task();
        task.subject = subject;
        task.OwnerId = id;
        task.Status='Active';
        task.Description=subject+ ' Description';

        return task;
    }
    
    public static Task NewTask(string subject, string description,Id Ownerid){
    
        Task task = new Task();
        task.subject = subject;
        task.OwnerId = Ownerid;
        task.Status='Active';
        task.Description= subject+'  '+ description; 
        return task;
    }
    
     public static Task NewTask(string subject,string description,Id Ownerid,Id relatedObjId){
    
        Task task = new Task();
        task.subject = subject;
        task.OwnerId = Ownerid;
        task.Status='Active';
        task.Description= subject+'  '+ description; 
        task.WhatId=relatedObjId;

        return task;
    }
    
     public static Task NewTask(string subject, string description,Id Ownerid,Id relatedObjId,Date DueDate){
    
        Task task = new Task();
        task.subject = subject;
        task.OwnerId = Ownerid;
        task.Status='Active';
        task.Description= subject+'  '+ description; 
        task.WhatId=relatedObjId;
        task.IsReminderSet=true;
        task.ActivityDate =DueDate;
        return task;
    }
    
    public static boolean IsExceededSampleRequstDuration(Integer durationDays,Date ValidateDate)
    {
        Integer RecordedDays;
        System.debug(' SAG IsExceededSampleRequstDuration Test...durationDays: ' +durationDays+' - ValidateDate :'+ValidateDate);
        if(ValidateDate!=null)
         {
             RecordedDays = (ValidateDate).daysBetween(Date.valueOf(system.today()));
            System.debug(' SAG IsExceededSampleRequstDuration Test...durationDays: ' +durationDays+' - RecordedDays '+RecordedDays );
             if(RecordedDays >durationDays)
                return true;
         }
            return false;
    }
    
    public static void DisplayDebugProcessDetails(Sample_Request__c objSampleRequest)
    {
        If(objSampleRequest!=null)
        {
            System.debug('SAG Checking objSampleRequest Details Name: '+objSampleRequest.Name);  
            System.debug('SAG Checking objSampleRequest status__c: '+objSampleRequest.status__c); 
            System.debug('SAG Checking objSampleRequest Is_Requested__c: '+objSampleRequest.Is_Requested__c); 
            System.debug('SAG Checking objSampleRequest Requested Date: '+objSampleRequest.Requested_Date__c);
            System.debug('SAG Checking objSampleRequest Is_Dispatched__c: '+objSampleRequest.Is_Dispatched__c);  
            System.debug('SAG Checking objSampleRequest Requested Date: '+objSampleRequest.Dispatched_Date__c); 
            System.debug('SAG Checking objSampleRequest Opportunity__r.OwnerID: '+objSampleRequest.Opportunity__r.OwnerID);  
        }   
    }
}