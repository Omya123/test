public with sharing class DiscussedTopicTriggerHandler {
    
    /***********************************************************************************
**********************************
* Author: Chitransh Porwal
* Date: 22/02/2019
* Param: trigger.new
* Return: void
* Description: This method invokes Before Insert and Create Opportunity along with OpportunityLineItem if Product is added to the record. 
* Summary of Changes :
*************************************************************************************
*********************************/
    public static void DiscussedTopicBeforeInsert(List<Discussed_Topic__c> discussedTopicList){
        
        if (discussedTopicList.size() > 0) {
            List<Opportunity> newOpportunitys = new List<Opportunity>();
            List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
            Map<String,Discussed_Topic__c> DiscussedTopicsByNameKeys = new Map<String,Discussed_Topic__c>();
            Map<String,OpportunityLineItem> OpportunityLineItemByNameKeys = new Map<String,OpportunityLineItem>();
            
            for (Discussed_Topic__c discTopic : discussedTopicList) {
                if(discTopic.Create_Opportunity__c == true){
                    String AccountString;
                    
                    /*  if(discTopic.AccountName__c !=null){		//----> Extract first 7 characters from Account Name for Opportunity Naming
Integer length = discTopic.AccountName__c.length();
if(length>7){
AccountString = discTopic.AccountName__c.substring(0, 7);
}
else{
AccountString = discTopic.AccountName__c;
}

}*/
                    if(discTopic.Material__c != null){
                        
                        String OpportunityNameForProd =  discTopic.Material_Name__c + System.now();// + AccountString  ;
                        DiscussedTopicsByNameKeys.put(OpportunityNameForProd,discTopic);
                        Opportunity opp = new Opportunity(name=OpportunityNameForProd,		    //---->Create New Opportunity
                                                          closeDate = discTopic.Visit_Date__c,
                                                          AccountId = discTopic.VisitAccount__c,
                                                          Pricebook2Id = discTopic.Price_Book__c,
                                                          stageName = system.label.OpportunityStage,
                                                          Visit_Report__c=discTopic.Visit_Report__c,
                                                          Primary_Contact__c  =discTopic.AttendeeContact__c
                                                         );
                        
                        List<PriceBookEntry> priceBookEntries = [SELECT Id, Product2Id, Product2.Id, Product2.Name,UnitPrice
                                                                 FROM PriceBookEntry WHERE
                                                                 Product2Id =: discTopic.Material__c AND Pricebook2ID =: discTopic.Price_Book__c LIMIT 1];
                        if(priceBookEntries.size()>0){
                            OpportunityLineItem oli = new OpportunityLineItem(); //---->Create OpportunityLineItem.
                            oli.PricebookEntryId = priceBookEntries[0].Id ;
                            oli.Quantity =Decimal.valueOf(system.label.Product_quantity);
                            oli.TotalPrice = Decimal.valueOf(system.label.Product_quantity) * priceBookEntries[0].UnitPrice;
                            OpportunityLineItemByNameKeys.put(OpportunityNameForProd,oli);
                        }
                        newOpportunitys.add(opp);
                        
                    }
                    if(discTopic.Need__c != null){
                        String OpportunityName = discTopic.Need__c ;//+ AccountString ;
                        if(discTopic.Raised_by__c != null){
                             
                             system.debug('sobjectType' + discTopic.AttendeeContact__c );
                            
                        }
                       
                        DiscussedTopicsByNameKeys.put(OpportunityName,discTopic);
                        Opportunity opp = new Opportunity(name=OpportunityName,     //---->Create New Opportunity
                                                          Need__c=discTopic.Need__c,
                                                          Pains__c=discTopic.Pains__c,
                                                          Gains__c=discTopic.Gains__c,
                                                          closeDate = discTopic.Visit_Date__c,
                                                          Situation__c=discTopic.Situation__c,
                                                          AccountId = discTopic.VisitAccount__c,
                                                          stageName = system.label.OpportunityStage,
                                                          Visit_Report__c=discTopic.Visit_Report__c,
														  Primary_Contact__c  =discTopic.AttendeeContact__c                                                           
                                                         );
                        newOpportunitys.add(opp);
                        
                    }
                }
                
            }
            if(newOpportunitys.size()>0){
                insert newOpportunitys;
            }
            
            for (Opportunity op : newOpportunitys) {           
                if (DiscussedTopicsByNameKeys.containsKey(op.Name)) {           
                    DiscussedTopicsByNameKeys.get(op.Name).Opportunity__c = op.Id;
                }
                if (OpportunityLineItemByNameKeys.containsKey(op.Name)) {
                    OpportunityLineItemByNameKeys.get(op.Name).OpportunityId = op.Id;
                    oliList.add(OpportunityLineItemByNameKeys.get(op.Name));
                }
            }
            if(oliList.size()>0){
                insert oliList;
            }
            
        }
        
        
    }
    /***********************************************************************************
**********************************
* Author: Chitransh Porwal
* Date: 19/02/2019
* Param: trigger.new
* Return: void
* Description: This method invokes After Insert  and It creates new record for Competitor Feedback.
* Summary of Changes :
*************************************************************************************
*********************************/
    public static void DiscussedTopicAfterInsert(List<Discussed_Topic__c> discussedTopicList){
        List<Competitor_feedback__c> competitorFeedbackList = new List<Competitor_feedback__c>();
        for(Discussed_Topic__c discTopic : discussedTopicList){
            if(discTopic.Competitor__c != null){
                Competitor_feedback__c competitorFeedbackItem = new Competitor_feedback__c();
                competitorFeedbackItem.Name = discTopic.Name + discTopic.Competitor_Name__c;
                competitorFeedbackItem.Competitor__c = discTopic.Competitor__c;
                competitorFeedbackItem.Competitor_Arguments__c = discTopic.Competitor_Arguments__c;
                competitorFeedbackItem.Material__c = discTopic.Material__c;
                competitorFeedbackItem.Comments__c = discTopic.Comments__c;
                competitorFeedbackList.add(competitorFeedbackItem);
            }
            if(competitorFeedbackList.size()>0){
                insert competitorFeedbackList;
            }
            
        }
        
    }
}