public with sharing class OpportunityService {

    public static void handleSegmentLogicAfterClosed(Map<Id,Opportunity> oppMap, Map<Id,Opportunity> oppOldMap) {

    	Set<id> closedWonOppsIdSet = new Set<Id>();

    	for(Opportunity opp : oppMap.values()){
    		if(opp.Stagename == 'Closed Won'){
    			closedWonOppsIdSet.add(opp.Id);
    		}
    	}


    	if(closedWonOppsIdSet.isEmpty()==false){
    		List<Segment__c> segmentList = [SELECT Id, Is_Realized__c, Opportunity__c, Opportunity__r.AccountId, Account__c FROM Segment__c WHERE Opportunity__c IN :closedWonOppsIdSet]; 

    		for(Segment__c segment : segmentList){
    			segment.Account__c = segment.Opportunity__r.AccountId;
    			segment.Is_Realized__c = true;
    		}

    		if(segmentList.isEmpty() == false){
    			update segmentList;
    		}

    	}
    }

    public static void createContractAfterClosedWon(Map<Id,Opportunity> oppMap, Map<Id,Opportunity> oppOldMap) {

    	List<Opportunity> closedWonOppsList = new List<Opportunity>();

    	for(Opportunity opp : oppMap.values()){
    		if(opp.Stagename == 'Closed Won'){
    			closedWonOppsList.add(opp);
    		}
    	}


    	if(closedWonOppsList.isEmpty()==false){
    		
    		for(Opportunity wonOpp : closedWonOppsList){
    			Contract newContract = new Contract();
    			newContract.AccountId = wonOpp.AccountId;
    			newContract.Status= 'Draft';
    			newContract.Pricebook2Id = wonOpp.PriceBook2Id;

    			insert newContract;
    			wonOpp.ContractId=newContract.Id;


    		}


    	}

    }
}