declare module "@salesforce/resourceUrl/GraphicsPackNew" {
    var GraphicsPackNew: string;
    export default GraphicsPackNew;
}
declare module "@salesforce/resourceUrl/LightningGraphicsPackExample" {
    var LightningGraphicsPackExample: string;
    export default LightningGraphicsPackExample;
}
declare module "@salesforce/resourceUrl/SLDS202forLtng" {
    var SLDS202forLtng: string;
    export default SLDS202forLtng;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/highlightjs" {
    var highlightjs: string;
    export default highlightjs;
}
declare module "@salesforce/resourceUrl/jqueryforltng" {
    var jqueryforltng: string;
    export default jqueryforltng;
}
declare module "@salesforce/resourceUrl/ok_for_investigation" {
    var ok_for_investigation: string;
    export default ok_for_investigation;
}
declare module "@salesforce/resourceUrl/warning_for_investigation" {
    var warning_for_investigation: string;
    export default warning_for_investigation;
}
